from .. import const as top_const
from . import const as ms_const


def __get_masterswitch_channel_name():
    if top_const.CHAMBER_TYPE in ['BSC_ST1', 'BSC_ST2'] :
        return ':ISI-' + top_const.CHAMBER + '_' + ms_const.MASTERSWITCH_CHANNEL_SUFFIX['ISI']
    elif top_const.CHAMBER_TYPE in ['HAM'] :
        return ms_const.MASTERSWITCH_CHANNEL_SUFFIX['ISI']
    elif top_const.CHAMBER_TYPE in ['HPI'] :
        return ms_const.MASTERSWITCH_CHANNEL_SUFFIX['HPI']
    else:
        raise Exception('Undefined behavior for reading a masterswitch channel on %s chamber type.' % top_const.CHAMBER_TYPE)


def is_masterswitch_off():
    return not is_masterswitch_on()


def is_masterswitch_on():
    #####
    # FIXME: these masterswitch checks were causing problems.  since
    # the manager is supposed to be handling the masterswitch, these
    # watchdog-like checks for the masterswitch being on or off were
    # preventing the manager to put the subordinates in the correct
    # state on restarts.  So just return True here so these checks
    # never fail.
    return True
    #####
    # FIXME: this is work around for a bug in the (EZ)CA library.
    # In order for the BSC ISI stages to check the master switch, they
    # must connect to an "IFO-rooted" channel, e.g.:
    #
    # ':ISI-' + top_const.CHAMBER + '_' + ms_const.MASTERSWITCH_CHANNEL_SUFFIX['ISI']
    #
    # However, there is currently a bug in the pyepics and/or ezca
    # libraries associated with connecting to IFO-rooted channels.  So
    # we need to bypass this in the BSC ISI stages until the problem
    # is fixed.
    if top_const.CHAMBER_TYPE in ['BSC_ST1', 'BSC_ST2'] :
        return True
    #####
    return ezca.read(__get_masterswitch_channel_name()) == ms_const.MASTERSWITCH_ON
