# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
# SVN $Id$
# $HeadURL$

from guardian import GuardState

from ezca.ligofilter import LIGOFilterManager
from ezca.ligofilter import LIGOFilterError
from ezca.ligofilter import Mask
from ezca.ezca import EzcaError

from .. import const as top_const
from .. import util as top_util
from . import const as iso_const
from . import util as iso_util
from .. import decorators as dec
from ..cartbias import CartesianBiasManager


# The defaultdict class is used in the states that engage isolation loops and the boost
# filters to track which of the filters we want to engage are actually loaded.
# The default value used for the dictionary that tracks this is of course the
# empty list.
#
# For more details on the defaultdict class, refer to
# http://docs.python.org/2/library/collections.html#collections.defaultdict
from collections import defaultdict


# We use chain.from_iterable() to flatten lists. See
# http://docs.python.org/2/library/itertools.html#itertools.chain.from_iterable
# for more details.
from itertools import chain


# create a state to wait for a specified sensor to settle.  this is
# based on the ISOLATION_CONSTANTS['ISO_OK_SENSOR'] value.  assumes
# the model has a sensor monitor channel named
# <ISO_OK_SENSOR>+'_MONITOR_OUT' that is zero if the sensor is
# settled, and not zero otherwise.  If ISO_OK_SENSOR is specified, a
# state will be created which waits until ISO_OK_SENSOR monitor
# channel is zero.  It is added to the graph before the isolation
# loops are engaged.
def get_iso_check_state(indicator):
    class ISO_CHECK(GuardState):
        request = False

        @dec.watchdog_is_not_tripped
        @dec.masterswitch_is_on
        @dec.damping_loops_are_in_preferred_state
        @dec.damping_loops_have_correct_gain
        def run(self):
            if ezca[indicator+'_MONITOR_OUT'] == 0:
                return True
    return ISO_CHECK


if 'ISO_OK_SENSOR' in iso_const.ISOLATION_CONSTANTS:
    globals()['WAIT_FOR_'+iso_const.ISOLATION_CONSTANTS['ISO_OK_SENSOR']+'_SETTLE'] = \
        get_iso_check_state(iso_const.ISOLATION_CONSTANTS['ISO_OK_SENSOR'])


# The most common state in this module is one that needs to change a the state
# of the isolation loops on a few degrees of freedom while the remaining
# degrees of freedom remain constant. The following function wraps a
# "state_factory" function to create such states for each level of isolation
# defined in iso_const.ISOLATION_CONSTANTS['LEVEL_FILTERS'].keys(). 
#
# add_isolation_states() adds states to the namespace of this module. The names
# of these states are determined by <state_prefix> and <to_change_dof_lists>.
# The provided <state_factory> should have the arguments (control_level,
# to_change, already_changed) where <control_level> specifies which control
# level the state will be used for, <to_change> specifies which degrees of
# freedom should change in the state, and <already_changed> specifies which of
# the isolation degrees of freedom have already been used in the <to_change>
# argument. The best way to understand why this function is useful is to
# explore its use to generate states in the code that follows.
def add_isolation_states(state_prefix, state_factory, to_change_dof_lists, already_changed_iso_dof_lists):
    for cl_name in iso_const.ISOLATION_CONSTANTS['LEVEL_FILTERS']:
        for i in range(len(to_change_dof_lists)):
            ## The following code is useful for debugging the names of the states that
            ## are being created...
            #print '{state_prefix}_{degs_of_freedom}_{control_level}_ISO{state_suffix}'.format(
            #    state_prefix=state_prefix,
            #    degs_of_freedom=iso_util.get_degs_of_freedom_string(to_change_dof_lists[i]),
            #    control_level=control_level,
            #    state_suffix=state_suffix)
            if len(to_change_dof_lists[i]) == 0:
                continue
            globals()['{state_prefix}_{control_level}_{degs_of_freedom}'.format(
                state_prefix=state_prefix,
                control_level=cl_name,
                degs_of_freedom=iso_util.get_degs_of_freedom_string(to_change_dof_lists[i]),
                )
                ] =\
                    state_factory(
                            control_level=cl_name,
                            to_change=to_change_dof_lists[i],
                            already_changed=list(chain.from_iterable(
                                already_changed_iso_dof_lists[:i])))
                            # ^^^ The use of chain.from_iterable() flattens a
                            # list of lists.
                            #
                            # Example:
                            # >>> from itertools import chain
                            # >>> list(itertools.chain.from_iterable([[1],[2]]))
                            # [1, 2]


class LOAD_CART_BIAS_FOR_ISOLATION(GuardState):
    request = False
    index = 99

    @dec.watchdog_is_not_tripped
    @dec.masterswitch_is_on
    @dec.damping_loops_are_in_preferred_state
    @dec.damping_loops_have_correct_gain
    @dec.isolation_loops_are_off
    def main(self):
        self.cbm = CartesianBiasManager(iso_const.DC_BIAS_DEVICE[top_const.CHAMBER_TYPE], ezca, deg_of_free_list=iso_const.ISOLATION_CONSTANTS['ALL_DOF'])
        self.has_requested_to_set_all_biases = False

    @dec.watchdog_is_not_tripped
    @dec.masterswitch_is_on
    @dec.damping_loops_are_in_preferred_state
    @dec.damping_loops_have_correct_gain
    @dec.isolation_loops_are_off
    def run(self):
        try:
            if not self.has_requested_to_set_all_biases:
                self.cbm.set_all_biases(ramp_time=0, wait=False)
                self.has_requested_to_set_all_biases = True

            return not self.cbm.is_ramping()
        except EzcaError as e:
            top_util.report_exception(e)
            return False


def get_restoring_cart_bias_state(control_level, to_change, already_changed):
    class RESTORE_CART_BIAS(GuardState):
        request = False

        @dec.watchdog_is_not_tripped
        @dec.masterswitch_is_on
        @dec.damping_loops_are_in_preferred_state
        @dec.damping_loops_have_correct_gain
        @dec.isolation_loops_are_in_preferred_state(control_level, already_changed, True)
        @dec.isolation_loops_have_correct_gain(already_changed)
        def main(self):
            self.cbm = CartesianBiasManager(iso_const.DC_BIAS_DEVICE[top_const.CHAMBER_TYPE], ezca, deg_of_free_list=to_change)
            self.has_requested_to_engage_offsets = False

            self.timer['wait'] = \
                iso_const.ISOLATION_CONSTANTS['CART_BIAS_RESTORE_OFFSET_RAMP_TIME'] \
                + iso_const.ISOLATION_CONSTANTS['CART_BIAS_RESTORE_POST_WAIT_TIME']
                
        @dec.watchdog_is_not_tripped
        @dec.masterswitch_is_on
        @dec.damping_loops_are_in_preferred_state
        @dec.damping_loops_have_correct_gain
        @dec.isolation_loops_are_in_preferred_state(control_level, already_changed, True)
        @dec.isolation_loops_have_correct_gain(already_changed)
        def run(self):
            try:
                if not self.has_requested_to_engage_offsets:
                    self.cbm.restore_all_offsets(
                            ramp_time=iso_const.ISOLATION_CONSTANTS['CART_BIAS_RESTORE_OFFSET_RAMP_TIME'],
                            wait=False)
                    self.has_requested_to_engage_offsets = True
                if self.cbm.is_ramping():
                    return 
                    
            except EzcaError as e:
                top_util.report_exception(e)
                return False

            if self.timer['wait']:
                return True

    return RESTORE_CART_BIAS


add_isolation_states('RESTORE_ISO_CART_BIAS',
        get_restoring_cart_bias_state,
        iso_const.ISOLATION_CONSTANTS['CART_BIAS_DOF_LISTS'],
        iso_const.ISOLATION_CONSTANTS['DOF_LISTS'],
                     )


def get_engaging_isolation_filters_state(control_level, to_change, already_changed):
    class _ENGAGE_ISO_FILTERS(GuardState):
        request = False

        @dec.watchdog_is_not_tripped
        @dec.masterswitch_is_on
        @dec.damping_loops_are_in_preferred_state
        @dec.damping_loops_have_correct_gain
        @dec.isolation_loops_have_correct_gain(already_changed)
        def main(self):
            self.filter_manager = LIGOFilterManager(['ISO_'+dof for dof in to_change], ezca)
            self.has_pressed_buttons = False
            self.loaded_filter_modules = defaultdict(list)

        @dec.watchdog_is_not_tripped
        @dec.masterswitch_is_on
        @dec.damping_loops_are_in_preferred_state
        @dec.damping_loops_have_correct_gain
        @dec.isolation_loops_have_correct_gain(already_changed)
        def run(self):
            try:
                if not self.has_pressed_buttons:
                    # If the following code is not clear, checkout the comments in
                    # the damping.states module.
                    for ligo_filter in self.filter_manager.filter_dict.values():
                        self.loaded_filter_modules[ligo_filter.filter_name] =\
                                top_util.get_loaded_filter_modules(ligo_filter, iso_const.ISOLATION_CONSTANTS['LEVEL_FILTERS'][control_level]['MAIN'])
                        if self.loaded_filter_modules[ligo_filter.filter_name]:
                            ligo_filter.only_on(*tuple(list(iso_const.ISOLATION_CONSTANTS['ONLY_ON_BUTTONS'])\
                                    + self.loaded_filter_modules[ligo_filter.filter_name]))
                    self.has_pressed_buttons = True

                for ligo_filter in self.filter_manager.filter_dict.values():
                    if self.loaded_filter_modules[ligo_filter.filter_name]:
                        for button in list(iso_const.ISOLATION_CONSTANTS['ONLY_ON_BUTTONS'])\
                                + self.loaded_filter_modules[ligo_filter.filter_name]:
                            if not ligo_filter.is_engaged(button):
                                return False
                return True

            except (LIGOFilterError, EzcaError) as e:
                top_util.report_exception(e)
                return False

    return _ENGAGE_ISO_FILTERS


add_isolation_states('ENGAGE_ISO_FILTERS',
        get_engaging_isolation_filters_state,
        iso_const.ISOLATION_CONSTANTS['DOF_LISTS'],
        iso_const.ISOLATION_CONSTANTS['DOF_LISTS'],
                     )


def get_ramping_isolation_filters_up_state(control_level, to_change, already_changed):
    class _RAMP_ISO_FILTERS_UP(GuardState):
        request = False

        @dec.watchdog_is_not_tripped
        @dec.masterswitch_is_on
        @dec.damping_loops_are_in_preferred_state
        @dec.damping_loops_have_correct_gain
        @dec.isolation_loops_are_in_preferred_state(control_level, already_changed, True)
        @dec.isolation_loops_have_correct_gain(already_changed)
        def main(self):
            self.filter_manager = LIGOFilterManager(['ISO_'+dof for dof in to_change], ezca)

            # break up ramping into sections (see below)
            self.has_started_ramping_gains = [False for i in range(len(iso_const.ISOLATION_CONSTANTS['RAMP_UP_TIMES']))]
            self.has_requested_to_turn_off_filters = False
            self.ramp_number = 0

        @dec.watchdog_is_not_tripped
        @dec.masterswitch_is_on
        @dec.damping_loops_are_in_preferred_state
        @dec.damping_loops_have_correct_gain
        @dec.isolation_loops_are_in_preferred_state(control_level, already_changed, True)
        @dec.isolation_loops_have_correct_gain(already_changed)
        def run(self):
            try:
                # Ramp down the deisolation gains in multiple steps.  This
                # is so we can provide a more appropriately smoother ramp
                # over the entire range
                if not self.has_started_ramping_gains[self.ramp_number]:
                    gain = iso_const.ISOLATION_CONSTANTS['RAMP_UP_GAINS'][self.ramp_number]
                    ramp_time = iso_const.ISOLATION_CONSTANTS['RAMP_UP_TIMES'][self.ramp_number]
                    self.filter_manager.ramp_gains(gain_value=gain, ramp_time=ramp_time, wait=False)
                    self.timer['isolating'] = ramp_time
                    self.has_started_ramping_gains[self.ramp_number] = True

                if not self.timer['isolating']:
                    return False

                if self.ramp_number < len(self.has_started_ramping_gains) - 1:
                    self.ramp_number += 1
                    return False

                if self.filter_manager.any_gain_ramping():
                    return False
                return True

            except (LIGOFilterError, EzcaError) as e:
                top_util.report_exception(e)
                return False

    return _RAMP_ISO_FILTERS_UP


add_isolation_states('RAMP_ISO_FILTERS_UP',
        get_ramping_isolation_filters_up_state,
        iso_const.ISOLATION_CONSTANTS['DOF_LISTS'],
        iso_const.ISOLATION_CONSTANTS['DOF_LISTS'],
                     )


def get_disengaging_boost_state(control_level, to_change, already_changed):
    # This function should not use the to_change and already_changed arguments,
    # so I overwrite them here for good measure. These arguments are included
    # so this function can be used in add_isolation_states().
    to_change = object()
    already_changed = object()

    class _DISENGAGE_BOOST(GuardState):
        request = False
    
        @dec.watchdog_is_not_tripped
        @dec.masterswitch_is_on
        @dec.damping_loops_are_in_preferred_state
        @dec.damping_loops_have_correct_gain
        @dec.isolation_loops_are_in_preferred_state(control_level, iso_const.ISOLATION_CONSTANTS['ALL_DOF'], True)
        @dec.isolation_loops_have_correct_gain(iso_const.ISOLATION_CONSTANTS['ALL_DOF'])
        def main(self):
            if (iso_const.ISOLATION_CONSTANTS['SWITCH_GS13_GAIN']) & (top_const.CHAMBER_TYPE != 'BSC_ST1') & (top_const.CHAMBER_TYPE != 'HPI'):
                iso_util.switch_gs13_gain('LOW',iso_const.ISOLATION_CONSTANTS['GS13_GAIN_DOF']) 
            if (top_const.CHAMBER_TYPE != 'HPI'):
                iso_util.turn_FF('OFF', iso_const.ISOLATION_CONSTANTS['FF_DOF'])
            self.filter_manager = LIGOFilterManager(['ISO_'+dof for dof in iso_const.ISOLATION_CONSTANTS['ALL_DOF']], ezca)
            self.has_pressed_buttons = False
    
        @dec.watchdog_is_not_tripped
        @dec.masterswitch_is_on
        @dec.damping_loops_are_in_preferred_state
        @dec.damping_loops_have_correct_gain
        @dec.isolation_loops_have_correct_gain(iso_const.ISOLATION_CONSTANTS['ALL_DOF'])
        def run(self):
            try:
                if not self.has_pressed_buttons:
                    self.filter_manager.all_do('turn_off', *iso_const.ISOLATION_CONSTANTS['LEVEL_FILTERS'][control_level]['BOOST'])
                    self.has_pressed_buttons = True
    
                # check that boost is disengaged on all the filters
                for ligo_filter in self.filter_manager.filter_dict.values():
                    for filter_name in iso_const.ISOLATION_CONSTANTS['LEVEL_FILTERS'][control_level]['BOOST']:
                        if ligo_filter.is_engaged(filter_name):
                            return False
                return True
    
            except (LIGOFilterError, EzcaError) as e:
                top_util.report_exception(e)
                return False

    return _DISENGAGE_BOOST


add_isolation_states('DISENGAGE_BOOST',
        get_disengaging_boost_state,
        (iso_const.ISOLATION_CONSTANTS['ALL_DOF'],),
        (iso_const.ISOLATION_CONSTANTS['ALL_DOF'],),
                     )


class DEISOLATING(GuardState):
    request = False
    index = 90

    @dec.watchdog_is_not_tripped
    @dec.masterswitch_is_on
    @dec.damping_loops_are_in_preferred_state
    @dec.damping_loops_have_correct_gain
    def main(self):     
        self.filter_manager = LIGOFilterManager(['ISO_'+dof for dof in iso_const.ISOLATION_CONSTANTS['ALL_DOF']], ezca)

        # break up ramping into sections (see below)
        self.has_started_ramping_gains = [False for i in range(len(iso_const.ISOLATION_CONSTANTS['RAMP_DOWN_TIMES']))]
        self.has_requested_to_turn_off_filters = False
        self.ramp_number = 0

    @dec.watchdog_is_not_tripped
    @dec.masterswitch_is_on
    @dec.damping_loops_are_in_preferred_state
    @dec.damping_loops_have_correct_gain
    def run(self):
        try:
            # Ramp down the deisolation gains in multiple steps.  This
            # is so we can provide a more appropriately smoother ramp
            # over the entire range
            if not self.has_started_ramping_gains[self.ramp_number]:
                gain = iso_const.ISOLATION_CONSTANTS['RAMP_DOWN_GAINS'][self.ramp_number]
                ramp_time = iso_const.ISOLATION_CONSTANTS['RAMP_DOWN_TIMES'][self.ramp_number]
                self.filter_manager.ramp_gains(gain_value=gain, ramp_time=ramp_time, wait=False)
                self.timer['deisolating'] = ramp_time
                self.has_started_ramping_gains[self.ramp_number] = True

            if not self.timer['deisolating']:
                return False

            if self.ramp_number < len(self.has_started_ramping_gains) - 1:
                self.ramp_number += 1
                return False

            if self.filter_manager.any_gain_ramping():
                return False

            if not self.has_requested_to_turn_off_filters:
                self.filter_manager.all_do('all_off', wait=True)
                self.has_requested_to_turn_off_filters = True

            for ligo_filter in self.filter_manager.filter_dict.values():
                if Mask() != ligo_filter.get_current_state_mask():
                    return False

        except (EzcaError, LIGOFilterError) as e:
            top_util.report_exception(e)
            return False

        return True


def get_engaging_isolation_boost_filters_state(control_level, to_change, already_changed):
    class _ENGAGE_ISO_BOOST(GuardState):
        request = False

        @dec.watchdog_is_not_tripped
        @dec.masterswitch_is_on
        @dec.damping_loops_are_in_preferred_state
        @dec.damping_loops_have_correct_gain
        @dec.isolation_loops_are_in_preferred_state(control_level, already_changed, True)
        @dec.isolation_loops_have_correct_gain(already_changed)
        def main(self):
            self.filter_manager = LIGOFilterManager(['ISO_'+dof for dof in to_change], ezca)
            self.has_pressed_buttons = False
            self.loaded_filter_modules = defaultdict(list)

        @dec.watchdog_is_not_tripped
        @dec.masterswitch_is_on
        @dec.damping_loops_are_in_preferred_state
        @dec.damping_loops_have_correct_gain
        @dec.isolation_loops_are_in_preferred_state(control_level, already_changed, True)
        @dec.isolation_loops_have_correct_gain(already_changed)
        def run(self):
            try:
                if not self.has_pressed_buttons:
                    # If the following code is not clear, checkout the comments in
                    # the damping.states module.
                    for ligo_filter in self.filter_manager.filter_dict.values():
                        self.loaded_filter_modules[ligo_filter.filter_name] =\
                                top_util.get_loaded_filter_modules(ligo_filter, iso_const.ISOLATION_CONSTANTS['LEVEL_FILTERS'][control_level]['BOOST'])
                        if self.loaded_filter_modules[ligo_filter.filter_name]:
                            ligo_filter.turn_on(*self.loaded_filter_modules[ligo_filter.filter_name])
                    self.has_pressed_buttons = True

                for ligo_filter in self.filter_manager.filter_dict.values():
                    for filter_name in self.loaded_filter_modules[ligo_filter.filter_name]:
                        if not ligo_filter.is_engaged(filter_name):
                            return False
                return True

            except (LIGOFilterError, EzcaError) as e:
                top_util.report_exception(e)
                return False

    return _ENGAGE_ISO_BOOST


add_isolation_states('ENGAGE_ISO_BOOST',
        get_engaging_isolation_boost_filters_state,
        iso_const.ISOLATION_CONSTANTS['DOF_LISTS'],
        iso_const.ISOLATION_CONSTANTS['DOF_LISTS'],
                     )
# Adding in an engage all to be used only after weve engaged all iso and
# then turned off the boosts
add_isolation_states('ENGAGE_ISO_BOOST',
                     get_engaging_isolation_boost_filters_state,
                     (iso_const.ISOLATION_CONSTANTS['ALL_DOF'],),
                     (iso_const.ISOLATION_CONSTANTS['ALL_DOF'],),
                     )


def get_isolation_idle_state(control_level, cust_index=False, default_boost=True):
    class _ISOLATED(GuardState):
        request = True
        if not cust_index:
            index = iso_const.ISOLATION_CONSTANTS['LEVEL_FILTERS'][control_level].get('INDEX', None)
        else:
            index = cust_index

        @dec.watchdog_is_not_tripped
        @dec.masterswitch_is_on
        @dec.damping_loops_are_in_preferred_state
        @dec.damping_loops_have_correct_gain
        @dec.isolation_loops_are_in_preferred_state(control_level, iso_const.ISOLATION_CONSTANTS['ALL_DOF'], default_boost)
        @dec.isolation_loops_have_correct_gain(iso_const.ISOLATION_CONSTANTS['ALL_DOF'])
        def main(self):
            self.has_requested_gs13_gain_switch = False
            self.has_requested_FF = False
        
        @dec.watchdog_is_not_tripped
        @dec.masterswitch_is_on
        @dec.damping_loops_are_in_preferred_state
        @dec.damping_loops_have_correct_gain
        @dec.isolation_loops_are_in_preferred_state(control_level, iso_const.ISOLATION_CONSTANTS['ALL_DOF'], default_boost)
        @dec.isolation_loops_have_correct_gain(iso_const.ISOLATION_CONSTANTS['ALL_DOF'])
        def run(self):
            if (iso_const.ISOLATION_CONSTANTS['SWITCH_GS13_GAIN']) & (self.has_requested_gs13_gain_switch == False) & (top_const.CHAMBER_TYPE != 'BSC_ST1') & (top_const.CHAMBER_TYPE != 'HPI'):
                    iso_util.switch_gs13_gain('HIGH', iso_const.ISOLATION_CONSTANTS['GS13_GAIN_DOF']) 
                    self.has_requested_gs13_gain_switch = True
            if (self.has_requested_FF == False):                    
                    if (top_const.CHAMBER_TYPE != 'HPI'):   
                        iso_util.turn_FF('ON', iso_const.ISOLATION_CONSTANTS['FF_DOF'])
                    self.has_requested_FF = True
            return True

    return _ISOLATED


def get_isolation_very_idle_state(control_level, cust_index=False, default_boost=True):
    class _ISOLATED_IDLE(GuardState):
        request = True
        if not cust_index:
            index = iso_const.ISOLATION_CONSTANTS['LEVEL_FILTERS'][control_level].get('INDEX', None) + 1
        else:
            index = cust_index

        @dec.watchdog_is_not_tripped
        @dec.masterswitch_is_on
        @dec.damping_loops_are_in_preferred_state
        @dec.damping_loops_have_correct_gain
        @dec.isolation_loops_are_in_preferred_state(control_level, iso_const.ISOLATION_CONSTANTS['ALL_DOF'], default_boost)
        @dec.isolation_loops_have_correct_gain(iso_const.ISOLATION_CONSTANTS['ALL_DOF'])
        def main(self):
            pass
        
        @dec.watchdog_is_not_tripped
        @dec.masterswitch_is_on
        @dec.damping_loops_are_in_preferred_state
        @dec.damping_loops_have_correct_gain
        @dec.isolation_loops_are_in_preferred_state(control_level, iso_const.ISOLATION_CONSTANTS['ALL_DOF'], default_boost)
        @dec.isolation_loops_have_correct_gain(iso_const.ISOLATION_CONSTANTS['ALL_DOF'])
        def run(self):
            return True

    return _ISOLATED_IDLE


for control_level in iso_const.ISOLATION_CONSTANTS['LEVEL_FILTERS']:
    globals()[control_level+'_ISOLATED'] = get_isolation_idle_state(control_level)
    # Add an isolated idle state but with boosts off
    globals()[control_level+'_ISOLATED_NO_BOOST'] = get_isolation_very_idle_state(control_level, cust_index=iso_const.ISOLATION_CONSTANTS['LEVEL_FILTERS'][control_level].get('INDEX', None)-20, default_boost=False)
